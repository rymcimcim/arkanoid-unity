﻿using UnityEngine;
using System.Collections;

public class BottomWall : MonoBehaviour {

	void OnTriggerEnter2D (Collider2D hitInfo) {
		if (hitInfo.name == "Ball") {
			GameSetup.Loose();
			hitInfo.gameObject.SendMessage("ResetBall");
		}
	}
}
