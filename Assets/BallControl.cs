﻿using UnityEngine;
using System.Collections;

public class BallControl : MonoBehaviour {
	
	private Rigidbody2D rigBody;
	public Transform player;

	public float ballSpeed = 0.10f;
	private Vector2 velocity;
	private Vector2 lastPos;

	void Awake() {
		rigBody = GetComponent<Rigidbody2D> ();
	}
	
	void Update() {
		Vector2 v = rigBody.velocity;
		float yVel = rigBody.velocity.y;
		if (yVel < ballSpeed && yVel > -ballSpeed && yVel != 0f) {
			if(yVel > 0f){
				v.y = ballSpeed;
				rigBody.velocity = v;
			} else{
				v.y = -ballSpeed;
				rigBody.velocity = v;
			}	
			//Debug.Log("Velocity before " + yVel);
			//Debug.Log ("Velocity after " + rigBody.velocity.y);
		}

	}

	void FixedUpdate ()
	{
		Vector3 pos3D = transform.position;
		Vector2 pos2D = new Vector2(pos3D.x, pos3D.y);

		velocity = pos2D - lastPos;
		lastPos = pos2D;
	}

	IEnumerator Start () {
		transform.position = new Vector2 (player.transform.position.x, player.transform.position.y + 0.15f);
		yield return new WaitForSeconds (3f);
		GoBall ();
	}

	void OnCollisionEnter2D(Collision2D col) {
		if (col.gameObject.name == "Player") {
			float x = hitFactor (transform.position,
			                  col.transform.position,
			                  ((BoxCollider2D)col.collider).size.x);

			Vector2 dir = new Vector2 (x, 1).normalized;

			rigBody.velocity = dir * ballSpeed;
		} else {
		
		Vector3 N = col.contacts[0].normal;

		Vector3 V = velocity.normalized;

		Vector3 R = Vector3.Reflect(V, N).normalized;

		rigBody.velocity = new Vector2(R.x, R.y) * ballSpeed;
		}
	}

	float hitFactor(Vector2 ballPos, Vector2 playerPos,
	                float playerWidth) {

		return (ballPos.x - playerPos.x) / playerWidth;
	}
	
	IEnumerator ResetBall()
	{
		rigBody.velocity = new Vector2(0f, 0f);
		transform.position = new Vector2 (player.transform.position.x, player.transform.position.y + 0.15f);
		yield return new WaitForSeconds(1f);
		GoBall();
	}
	
	public void GoBall() {
		rigBody = GetComponent<Rigidbody2D> ();

			float randomNumber = Random.Range (0, 2);
			if (randomNumber <= 0.5) {
				rigBody.AddForce (new Vector2 (ballSpeed, 10f));
			} else {
				rigBody.AddForce (new Vector2 (-ballSpeed, -10f));
			}

	}
}