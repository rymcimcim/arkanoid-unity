﻿using UnityEngine;
using System.Collections;

public class GameSetup : MonoBehaviour {
	
	public Camera mainCam;
	
	public BoxCollider2D topWall;
	public BoxCollider2D bottomWall;
	public BoxCollider2D leftWall;
	public BoxCollider2D rightWall;
	
	public Transform player01;
	
	public static int playerLife = 5;
	public static int playerScore = 0;
	
	public GUISkin theSkin;
	
	public Transform thaBall;

	void Awake(){
		thaBall = GameObject.FindGameObjectWithTag ("Ball").transform;
	}

	void Update () {

		topWall.size = new Vector2 (mainCam.ScreenToWorldPoint (new Vector3 (Screen.width * 2f, 0f, 0f)).x, 1f);
		topWall.offset = new Vector2 (0f, mainCam.ScreenToWorldPoint (new Vector3 (0f, Screen.height, 0f)).y + 0.5f);
		
		bottomWall.size = new Vector2 (mainCam.ScreenToWorldPoint (new Vector3 (Screen.width * 2f, 0f, 0f)).x, 1f);
		bottomWall.offset = new Vector2 (0f, mainCam.ScreenToWorldPoint (new Vector3 (0f, 0f, 0f)).y - 0.5f);
		
		leftWall.size = new Vector2 (1f, mainCam.ScreenToWorldPoint(new Vector3(0f, Screen.height * 2f, 0f)).y);
		leftWall.offset = new Vector2 (mainCam.ScreenToWorldPoint (new Vector3 (0f, 0f, 0f)).x - 0.5f, 0f);
		
		rightWall.size = new Vector2 (1f, mainCam.ScreenToWorldPoint(new Vector3(0f, Screen.height * 2f, 0f)).y);
		rightWall.offset = new Vector2 (mainCam.ScreenToWorldPoint (new Vector3 (Screen.width, 0f, 0f)).x + 0.5f, 0f);

		gameOver ();
	}

	public static void gameOver() {
		if (playerLife == 0) {
			Application.Quit ();
		} else if (playerScore == 700) {
			Application.Quit();
		}
	}

	public static void Loose () {
			playerLife -= 1;
	}

	public static void Score(string BlockTag){
		if (BlockTag == "BlockBlue") {
			playerScore += 10;
		} else if (BlockTag == "BlockGreen") {
			playerScore += 50;
		} else if (BlockTag == "BlockOrange") {
			playerScore += 100;
		}
	}

	public void OnGUI() {
		GUI.skin = theSkin;
		GUI.Label (new Rect(Screen.width/2-150-18, 20, 100, 100),"Life: " + playerLife);
		GUI.Label (new Rect(Screen.width/2+150+18, 20, 100, 100),"Score: " + playerScore);
		
		if (GUI.Button (new Rect (Screen.width / 2 - 121 / 2, 35, 121, 53), "Quit")) {
			Application.Quit();
		}
	}
}
