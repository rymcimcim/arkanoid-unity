﻿using UnityEngine;
using System.Collections;

public class PlayerControlls : MonoBehaviour {

	public KeyCode moveLeft;
	public KeyCode moveRight;

	public float speed = 2.5f;

	private Rigidbody2D rigBody;
	
	void Awake () {
		rigBody = GetComponent<Rigidbody2D> ();
	}

	void Update () {
		if (Input.GetKey (moveLeft)) {
			rigBody.velocity = new Vector2(-speed * Time.deltaTime, 0f);
		} else if (Input.GetKey (moveRight)) {
			rigBody.velocity = new Vector2(speed * Time.deltaTime, 0f);
		} else {
			rigBody.velocity = new Vector2(0f, 0f);
		}
	}
}
