﻿using UnityEngine;
using System.Collections;

public class BlocksBehaviour : MonoBehaviour {

	void OnCollisionEnter2D (Collision2D hitInfo) {
		if (hitInfo.collider.name == "Ball") {
			string BlockTag = transform.tag;
			GameSetup.Score(BlockTag);
			Destroy(gameObject);
		}
	}
}